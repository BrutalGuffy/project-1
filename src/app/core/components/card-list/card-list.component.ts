import { Component, OnInit, EventEmitter } from '@angular/core';
import {forEach} from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.css']
})
export class CardListComponent implements OnInit {

  cards = [];
  cardsToRenderUndone = [];
  cardsToRenderDone = [];

  constructor() {
  }

  ngOnInit() {

  }

  onFormSubmit(form) {
    const card = form.value;
    this.cards.push(card);
    this.cardsToRenderUndone.push(card);
    console.log(this.cards);

  }

  onSearch(value) {
    const searchCombined = {
      search: value[0],
      checkbox: value[1]
    };

    const searchedCardsUndone = this.cardsToRenderUndone.filter(card => {
      return (card.tittle.match(searchCombined.search)) && (card.isDone === searchCombined.checkbox);
    });

    const searchedCardsDone = this.cardsToRenderDone.filter(card => {
      return (card.tittle.match(searchCombined.search)) && (card.isDone === searchCombined.checkbox);
    });

    this.renderUndone(searchedCardsUndone);
    this.renderDone(searchedCardsDone);
  }

  onCardClicked(value) {

    const newCardsUndone = [];
    const newCardsDone = [];

    this.cards.forEach(function (card) {
      if (card.tittle === value) {

        if (card.isDone === true) {
          card.isDone = false;
          return 0;
        }

        card.isDone = true;

      }
    });

    this.cards.forEach( function (card) {
      if (card.isDone === false) {
        newCardsUndone.push(card);
      } else {
        newCardsDone.push(card);
      }
    });

    this.renderUndone(newCardsUndone);
    this.renderDone(newCardsDone);
    console.log(this.cards);

  }

  renderUndone(array) {
    this.cardsToRenderUndone = array;
  }

  renderDone(array) {
    this.cardsToRenderDone = array;
  }
}
