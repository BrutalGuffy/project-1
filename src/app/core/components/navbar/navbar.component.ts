import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {debounceTime, mergeMap, takeUntil} from 'rxjs/operators';
import {combineLatest, merge, Subject} from 'rxjs';

const DEBOUNCE_TIME_SEARCH = 500;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {

  @Output() onSearch = new EventEmitter<any>();

  search = new FormControl('');

  checkbox = new FormControl('');

  destroy$ = new Subject<void>();


  constructor() { }

  ngOnInit() {

    const search$ = this.search.valueChanges.pipe(
      debounceTime(DEBOUNCE_TIME_SEARCH)
    );

    const check$ = this.checkbox.valueChanges.pipe();

    const combineSearch$ = combineLatest(search$, check$);

    combineSearch$.subscribe(value => {
      this.onSearch.emit(value);
    });

    this.search.setValue('');
    this.checkbox.setValue(false);
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

}
