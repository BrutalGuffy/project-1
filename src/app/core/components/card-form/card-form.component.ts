import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {detectChanges} from '@angular/core/src/render3';

@Component({
  selector: 'app-card-form',
  templateUrl: './card-form.component.html',
  styleUrls: ['./card-form.component.css']
})

export class CardFormComponent implements OnInit {

  @Output() onFormSubmit = new EventEmitter<any>();
  cardForm = new FormGroup({
    tittle: new FormControl(''),
    content: new FormControl(''),
    isDone: new FormControl(false),
  });

  visibility = false;
  isValid = false;

  constructor() { }

  ngOnInit() {
  }

  toggleMenu() {
    this.visibility = !this.visibility;
    this.cardForm.reset();
    this.isValid = false;
  }

  onSubmit() {
    if (this.cardForm.invalid
      || (this.cardForm.value.tittle === null)
      || (this.cardForm.value.tittle === '')) {
      this.isValid = true;
      return;
    }
    this.cardForm.value.isDone = false;
    console.log(this.cardForm.value);
    this.onFormSubmit.emit(this.cardForm);
    this.visibility = !this.visibility;
    this.cardForm.reset();
  }

  onChange() {
    this.isValid = false;
  }

}
