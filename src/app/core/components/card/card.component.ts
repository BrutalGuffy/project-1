import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() tittle: string = null;
  @Input() content: string = null;
  @Input() done = false;
  @Output() onCardClicked = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {

  }

  onClick() {
    this.done = true;
    this.onCardClicked.emit(this.tittle);
  }

}


